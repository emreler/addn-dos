FROM node:8-slim

WORKDIR /app

COPY . /app
RUN npm install

ENTRYPOINT [ "npx", "ts-node", "dos.ts" ]
