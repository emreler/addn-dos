import { expect } from 'chai';
import 'mocha';

import addN from '../addn';

describe('AddN function', () => {
    it('should add numbers correctly', () => {
        const addEight = addN(8);
        expect(addEight(7)).to.equal(15);
        expect(addEight(100)).to.equal(108);
    });
});
