# Dott Assignment
This is the source code repository for the Dott Assignment.

# Running
## Locally
Note: It's been developed using Node.js version `8.9.1` and npm version `5.5.1`.
First install dependencies:
```
npm install
```
There are 2 assignments in this project: "AddN" (Task 1), and "DoS" (Task 2). Commands to run them are like the following:

**AddN**:
```
npx ts-node dos.ts google.com --type=(tcp|udp)
```

**DoS**
```
npx ts-node dos.ts google.com --type=(tcp|udp)
```

## Test
There is currently minimal unit test only covering the `AddN`. To run the test, use the following command:
```
npm test
```
## Inside Docker container
Run the following to build the image
```
docker build -t dos-assignment .
```
and then run for running the DoS:
```
docker run -ti dos-assignment google.com --type=(tcp|udp)
```

Note that Ctrl-C doesn't work with this way of working. Need to run with `-it` flags to attach the TTYs.
