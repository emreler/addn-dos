export default (x: number) => {
    /** Returns a function that adds N to the given parameter */
    return function(y: number) {
        return x + y;
    }
}
